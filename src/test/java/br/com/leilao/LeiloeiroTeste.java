package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class LeiloeiroTeste {

    Usuario usuario;
    Lance lance;
    Leilao leilao;
    Leiloeiro leiloeiro;

    @BeforeEach
    public void setup() {
        this.usuario = new Usuario(1, "Mary Padian");
        this.lance = new Lance(usuario, 400.00);
        Lance lanceMenor = new Lance(usuario, 100.00);

        ArrayList<Lance> lances = new ArrayList<>();
        lances.add(lance);
        lances.add(lanceMenor);

        this.leilao = new Leilao(lances);

        this.leiloeiro = new Leiloeiro("Laura Dotson", leilao);
    }

    @Test
    public void testarRetornarMaiorLance() {
        Lance lanceMaior = leiloeiro.retornarMaiorLance();

        Assertions.assertSame(this.lance, lanceMaior);
    }

    @Test
    public void testarRetornarMaiorLanceListaVazia() {
        ArrayList<Lance> lances = new ArrayList<>();
        leilao.setLances(lances);
        leiloeiro.setLeilao(leilao);

        Assertions.assertThrows(RuntimeException.class, () -> {leiloeiro.retornarMaiorLance();});
    }

}
