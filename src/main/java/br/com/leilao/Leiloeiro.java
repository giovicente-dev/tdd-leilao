package br.com.leilao;

import java.util.ArrayList;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() {
        ArrayList<Lance> lances = leilao.getLances();

        if (lances.isEmpty()) {
            throw new RuntimeException("Não existem lances até o momento");
        }

        Lance lanceMaior = lances.get(0);

        for (Lance lanceLista : lances) {
            if (lanceMaior.getValor() < lanceLista.getValor()) {
                lanceMaior = lanceLista;
            }
        }

        return lanceMaior;
    }
}
