package br.com.leilao;

public class Usuario {

    private int id;
    private String nome;

    public Usuario() { }

    public Usuario(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }
}
