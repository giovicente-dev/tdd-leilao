package br.com.leilao;

import java.util.ArrayList;

public class Leilao {

    private ArrayList<Lance> lances;

    public Leilao() {
    }

    public Leilao(ArrayList<Lance> lances) {
        this.lances = lances;
    }

    public ArrayList<Lance> getLances() {
        return lances;
    }

    public void setLances(ArrayList<Lance> lances) {
        this.lances = lances;
    }

    public Lance validarLance(Lance lance) {
        for (Lance lanceLista : this.lances) {
            if (lance.getValor() < lanceLista.getValor()) {
                throw new RuntimeException("Lance menor que o anterior.");
            }
        }
        return lance;
    }

    public Lance adicionarLance(Lance lance) {
        Lance lanceValido = validarLance(lance);
        this.lances.add(lanceValido);

        return lanceValido;
    }

}
