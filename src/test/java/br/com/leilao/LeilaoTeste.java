package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class LeilaoTeste {
    Usuario usuario;
    Lance lance;
    Leilao leilao;

    @BeforeEach
    public void setup() {
        this.usuario = new Usuario(1, "Mary Padian");
        this.lance = new Lance(usuario, 400.00);

        ArrayList<Lance> lances = new ArrayList<>();
        lances.add(lance);

        this.leilao = new Leilao(lances);
    }

    @Test
    public void testarValidarLancePositivo() {

        Lance lance = new Lance(this.usuario, 500.00);
        Lance lanceResposta = leilao.validarLance(lance);

        Assertions.assertSame(lance, lanceResposta);
    }

    @Test
    public void testarValidarLanceNegativo() {
        Lance lance = new Lance(this.usuario, 200.00);
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.validarLance(lance);});
    }

    @Test
    public void testarAdicionarLancePositivo() {
        Lance lance = new Lance(this.usuario, 500.00);
        Lance lanceResposta = leilao.adicionarLance(lance);

        Assertions.assertSame(lance, lanceResposta);
    }

    @Test
    public void testarAdicionarLanceNegativo() {
        Lance lance = new Lance(this.usuario, 200.00);
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarLance(lance);});
    }

}
